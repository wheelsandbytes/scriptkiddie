#!/bin/bash
IPADDR="$(ifconfig en0 | grep -oE '\d+\.\d+\.\d+\.\d+ ')"
ETHERNETIP="$(ifconfig en1 | grep -oE '\d+\.\d+\.\d+\.\d+ ')"
# echo "${ETHERNETIP}"
if [ -n "${IPADDR}" ]; then
    echo "Connected to Wi-Fi with IP ${IPADDR}"
else
    echo "Wi-Fi disconnected"
fi
if [ -n "${ETHERNETIP}" ]; then
    echo "Connected to Ethernet with IP ${ETHERNETIP}"
else
    echo "Ethernet disconnected"
fi
