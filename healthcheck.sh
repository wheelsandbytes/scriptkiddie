#!/bin/bash

STATUS="$(curl -s http://yourhosthere.com/healthcheck)"

if [ "${STATUS}" == "UP" ]; then
    echo "host is UP"
else
    echo "host is DOWN"
fi
