#!/bin/bash

# when something get stuck just reboot it
# feel free to add more victims >:)

victims="Finder Dock SystemUIServer cfprefsd"

for victim in $victims; do
  echo "restarting ${victim}"
  killall "${victim}"
done
