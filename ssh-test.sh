#!/bin/bash
USERNAME=${USER}
HOSTS="host.domain.com"

# echo $HOSTS

# script to send via ssh
SCRIPT="pwd; ls; whoami; uname -a"

for HOSTNAME in ${HOSTS} ; do
   ssh -o StrictHostKeyChecking=no -l ${USERNAME} ${HOSTNAME} "${SCRIPT}"
   echo $HOSTNAME
done
